const mongoose = require('mongoose');
const {values} = require('lodash');
const {EMonster} = require('./monster.enum');

const MonsterSchema = new mongoose.Schema({
  type: {
    type: String,
    enum: values(EMonster),
    required: true
  },
});

module.exports = mongoose.model('Monster', MonsterSchema);
