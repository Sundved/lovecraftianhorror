const fs = require('fs');
const express = require('express');
const expressJwt = require('express-jwt');
const fileCtrl = require('./file.controller');
const config = require('../../config/config');
const multer = require('multer');
const Grid = require('gridfs-stream');
const {mongo, connection, Types} = require('mongoose');

Grid.mongo = mongo;

const router = express.Router(); // eslint-disable-line new-cap

connection.on('open', () => {
  const gfs = Grid(connection.db);
  const storage = multer.diskStorage({
    destination: function (req, file, cb) {
      cb(null, `${config.fileStorage}/${req.user._id}`)
    },
    filename: function (req, file, cb) {
      cb(null, 'avatar' + req.user.id + Date.now())
    }
  })
  // sets file input to single file
  const singleUpload = multer({
    storage: storage,
    limits: { fieldSize: 25 * 1024 * 1024 },
    fileFilter: (req, file, cb) => {
      if (
        file.mimetype !== 'image/png' &&
        file.mimetype !== 'image/jpeg' &&
        file.mimetype !== 'image/gif'
      ) {
        cb(new Error('Not resolved extension'));
      } else {
        cb(null, true);
      }
  }}).single('file');

  router.route('/avatar')
  .post(expressJwt({ secret: config.jwtSecret }),
  fileCtrl.createDirectories,
  singleUpload,
  fileCtrl.upload
  );

  router.route('/:id')
    .get((req, res) => {
      gfs.files.findOne({_id: Types.ObjectId(req.params.id)}, (err, file) => {
        if (!file) {
          return res.status(404).json({
            message: 'Could not find file'
          })
        }
        var readstream = gfs.createReadStream({
          _id: file._id
        })
        res.set('Content-Type', file.contentType)
        return readstream.pipe(res)
      })
    })

  router.get('/', (req, res) => {
    gfs.files.find().toArray((err, files) => {
      if (!files || files.length === 0) {
        return res.status(404).json({
          message: 'Could not find files'
        })
      }
      return res.json(files)
    })
  });

  router.route('/upload')
    .post(expressJwt({ secret: config.jwtSecret }), fileCtrl.upload);
});

module.exports = router;
