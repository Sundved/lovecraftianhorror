const User = require('../user/user.model');
const config = require('../../config/config');
const fs = require('fs');
const {RandomHash} = require('random-hash');
const sharp = require('sharp');

const generateHash = new RandomHash();

function createDirectories (req, res, next) {
  const userDir = `${config.fileStorage}/${req.user._id}`;
  if (!fs.existsSync(config.fileStorage)){
    fs.mkdirSync(config.fileStorage);
  }
  if (!fs.existsSync(userDir)){
    fs.mkdirSync(userDir);
  }
  next();
}

function upload (req, res, next) {
  let uniqueName;
  do {
    uniqueName = generateHash().toLowerCase() + '.png'
  } while (fs.existsSync(`${req.file.destination}/${uniqueName}`))
  sharp(req.file.path)
    .resize({ height: 300 })
    .png()
    .toFile(`${req.file.destination}/${uniqueName}`, (err) => {
      if (fs.existsSync(req.file.path)) {
        fs.unlink(req.file.path);
      }
      if (err) {
        return next(err)
      } else {
        User.get(req.user._id).then(user => {
          if (user.avatar) {
            const prevAvatar = `${config.fileStorage}/..${user.avatar}`
            if (fs.existsSync(prevAvatar)) {
              fs.unlink(prevAvatar)
            }
          }
          const avatar = `/upload/${req.user._id}/${uniqueName}`;
          User.findOneAndUpdate({ _id: req.user._id }, { avatar }).then(() => {
            res.json({ avatar })
          })
        })
      }
    })
}

module.exports = { upload, createDirectories };
