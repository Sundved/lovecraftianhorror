const {random, forEach} = require('lodash');

const weightedRandom = (elements) => {
  const r = random(1, 100);
  let sum = 0;

  for (let i=0;i<elements.length;i++) {
    const element = elements[i];
    sum += element.weight;
    if (r <= sum) return element.value;
  }
}
