const {values} = require('lodash');
const mongoose = require('mongoose');
const {EDirection} = require('./level.enum');

const DirectionSchema = new mongoose.Schema({
  direction: {
    type: String,
    enum: values(EDirection)
  },
  linkId: mongoose.Schema.Types.ObjectId
});

module.exports = mongoose.model('Direction', DirectionSchema);
