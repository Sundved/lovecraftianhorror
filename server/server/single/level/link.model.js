const mongoose = require('mongoose');

const LinkSchema = new mongoose.Schema({
  locationModel: {
    type: String,
    enum: ['Battle', 'Portal']
  },
  location: {
    type: mongoose.Schema.Types.ObjectId,
    refPath: 'locationType',
  },
  directions: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Direction',
  }]
});

module.exports = mongoose.model('Link', LinkSchema);
