const {values} = require('lodash');
const mongoose = require('mongoose');
const {ELocation} = require('../../location/location.enum');
const {EOffset} = require('./level.enum');

const LevelSchema = new mongoose.Schema({
  number: {
    type: Number,
    required: true
  },
  links: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Link',
  }]
});

module.exports = mongoose.model('Level', LevelSchema);
