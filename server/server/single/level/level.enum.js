const EDirection = {
  LEFT: 'LEFT',
  UP: 'UP',
  RIGHT: 'RIGHT'
};

module.exports = {EDirection};
