const {random, isEmpty, compact, sample, forEach, values, difference, includes, forEachRight} = require('lodash');
const Single = require('./single.model');
const Level = require('./level/level.model');
const Link = require('./level/link.model');
const Direction = require('./level/direction.model');
const {ELocation} = require('../location/location.enum');
const {EDirection, EOffset} = require('./level/level.enum');
const {getCharacter} = require('../character/character.factory');

const levelsCount = 3;
const locationsCount = {
  [ELocation.BATTLE]: 10,
  [ELocation.PORTAL]: 5
}
const minInitialLinksCount = 3;
const maxLinksCount = 5;

const locations = [
  {
    value: ELocation.BATTLE,
    weight: 100
  },
  {
    value: ELocation.PORTAL,
    weight: 0
  }
]

const getLink = () => {
  return new Link({

  });
}

const getDirection = (direction, link) => {
  return new Direction({
    direction,
    linkId: link._id
  });
}

const linkLinks = (links, prevLinks) => {
  const diff = prevLinks.length - links.length;
  let canAddToLeft = true;
  forEach(prevLinks, (link, index) => {
    let directions = [];
    const prevLink = prevLinks[index - 1];
    const nextLink = prevLinks[index + 1];
    if (diff > 0) {
      if (!prevLink) {
        canAddToLeft = false;
        directions.push(getDirection(EDirection.RIGHT, links[index]));
      }
      if (!nextLink) {
        directions.push(getDirection(EDirection.LEFT, links[index - 2]));
      }
      if (prevLink && nextLink) {
        directions.push(getDirection(EDirection.UP, links[index - 1]));
        if (canAddToLeft) {
          directions.push(getDirection(EDirection.LEFT, links[index - 2]));
        } else {
          canAddToLeft = true;
        }
        if (links[index] && random(0, 1)) {
          canAddToLeft = false;
          directions.push(getDirection(EDirection.RIGHT, links[index]));
        }
      }
    } else {
      if (!prevLink) {
        directions.push(getDirection(EDirection.LEFT, links[index]));
      }
      if (!nextLink) {
        directions.push(getDirection(EDirection.RIGHT, links[index + 2]));
      }
      directions.push(getDirection(EDirection.UP, links[index + 1]));
      if (prevLink && nextLink) {
        if (canAddToLeft) {
          directions.push(getDirection(EDirection.LEFT, links[index]));
        } else {
          canAddToLeft = true;
        }
        if (links[index + 2] && random(0, 1)) {
          canAddToLeft = false;
          directions.push(getDirection(EDirection.RIGHT, links[index + 2]));
        }
      }
    }
    link.directions = directions;
  });
  forEach(prevLinks, link => forEach(link.directions, direction => direction.save()));
}

const getLevel = (number, prevLevel) => {
  let linksCount = 3;
  if (prevLevel) {
    switch (prevLevel.links.length) {
      case 1:
        break;
      case 3:
        linksCount = 5;
        break;
      case 5:
        linksCount = 3;
        break;
    }
  }

  let links = [];

  for(let i = 0; i < linksCount; i++) {
    links.push(getLink());
  }

  prevLevel && linkLinks(links, prevLevel.links);
  forEach(links, link => link.save());

  const level = new Level({
    number,
    links,
  });
  level.save();

  return level;
}

const getSingle = (user, characterType) => {
  let levels = [];
  for(let i = 0; i < levelsCount; i++) {
    const prevLevel = levels[0];
    levels.unshift(getLevel(i+1, prevLevel));
  }

  return new Single({
    userId: user._id,
    character: getCharacter(characterType),
    levels
  });
};

module.exports = {getSingle};
