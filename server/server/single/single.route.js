const express = require('express');
const expressJwt = require('express-jwt');
const validate = require('express-validation');
const paramValidation = require('../../config/param-validation');
const userCtrl = require('../user/user.controller');
const gameCtrl = require('./single.controller');
const config = require('../../config/config');

const router = express.Router(); // eslint-disable-line new-cap

router.route('/')
  /** POST /api/single - Create new game */
  .post(
    validate(paramValidation.createSingle),
    expressJwt({ secret: config.jwtSecret }),
    userCtrl.load,
    gameCtrl.create
  )
  .delete(
    expressJwt({ secret: config.jwtSecret }),
    gameCtrl.defeat
  );
router.route('/:singleId')
  .get(
    expressJwt({ secret: config.jwtSecret }),
    gameCtrl.load
  )


module.exports = router;
