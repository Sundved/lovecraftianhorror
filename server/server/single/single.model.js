const mongoose = require('mongoose');
const httpStatus = require('http-status');
const APIError = require('../helpers/APIError');
const Level = require('./level/level.model');

/**
 * User Schema
 */
const Single = new mongoose.Schema({
  userId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
    required: true
  },
  character: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Character',
    required: true
  },
  levels: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Level'
}],
  status: {
    type: String,
    enum: ['ACTIVE', 'WIN', 'DEFEAT'],
    default: 'ACTIVE'
  },
  createdAt: {
    type: Date,
    default: Date.now
  }
});

Single.statics = {
  getActive(userId) {
    return this.findOne({userId, status: 'ACTIVE'})
      .exec()
      .then((game) => {
        if (game) {
          return game;
        }
        const err = new APIError('No such game exists!', httpStatus.NOT_FOUND);
        return Promise.reject(err);
      });
  }
}

module.exports = mongoose.model('Single', Single);
