const Single = require('./single.model');
const User = require('../user/user.model');
const {getSingle} = require('./single.factory');

function load(req, res, next) {
  Single.findOne({
    _id: req.params.singleId,
    userId: req.user._id,
    status: 'ACTIVE',
  }).populate({
    path: 'character',
    populate: {path: 'cards'}
  }).populate({
    path: 'levels',
    populate: {path: 'links', populate: {path: 'directions'}}
  }).exec((err, game) => {
    res.json(game)
  })
    .catch(e => next(e));
}

function create(req, res, next) {
  const game = getSingle(req.user, req.body.character);

  return game.save()
    .then(savedGame => {
      User.setActiveGame(req.user._id, game._id);
      return res.json(savedGame)
    })
    .catch(e => next(e));
}

function defeat(req, res, next) {
  Single.update({userId: req.user._id, status: 'ACTIVE'}, {
    status: 'DEFEAT'
  }).then(games => {
    User.unsetActiveGame(req.user._id);
    res.json(games)
  });
}

module.exports = {load, create, defeat};
