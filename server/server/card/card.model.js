const mongoose = require('mongoose');
const {values} = require('lodash');
const {ECard} = require('./card.enum');
const {ERarity} = require('../common/common.enum');
const {ECharacter} = require('../character/character.enum');

const CardSchema = new mongoose.Schema({
  type: {
    type: String,
    enum: values(ECard),
    required: true
  },
  rarity: {
    type: String,
    enum: values(ERarity),
    required: true,
    default: ERarity.COMMON
  },
  owner: {
    type: String,
    enum: values(ECharacter)
  }
});

module.exports = mongoose.model('Card', CardSchema);
