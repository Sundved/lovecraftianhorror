const jwt = require('jsonwebtoken');
const httpStatus = require('http-status');
const APIError = require('../helpers/APIError');
const config = require('../../config/config');
const User = require('../user/user.model');

// sample user, used for authentication
const user = {
  username: 'react',
  password: 'express'
};

/**
 * Returns jwt token if valid username and password is provided
 * @param req
 * @param res
 * @param next
 * @returns {*}
 */
function login(req, res, next) {
  User.findOne({username: req.body.username}, (err, user) => {
    if (err) throw err;

    if (!user) {
      return next(new APIError('Authentication error', httpStatus.UNAUTHORIZED, true));
    }

    // test a matching password
    user.comparePassword(req.body.password, function(err, isMatch) {
      if (err) throw err;

      if (!isMatch) {
        return next(new APIError('Authentication error', httpStatus.UNAUTHORIZED, true));
      }

      const token = jwt.sign({
        _id: user._id,
        username: user.username
      }, config.jwtSecret);
      return res.json({
        token,
        userId: user._id,
      });
    });
  });
}

/**
 * This is a protected route. Will return random number only if jwt token is provided in header.
 * @param req
 * @param res
 * @returns {*}
 */
function getRandomNumber(req, res) {
  // req.user is assigned by jwt middleware if valid token is provided
  return res.json({
    user: req.user,
    num: Math.random() * 100
  });
}

module.exports = { login, getRandomNumber };
