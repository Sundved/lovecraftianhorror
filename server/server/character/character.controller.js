const Character = require('./character.model');
const {getCharacter} = require('./character.factory');
const {ECharacter} = require('./character.enum');

function list(req, res) {
  res.json([
    getCharacter(ECharacter.JACK),
    getCharacter(ECharacter.KATE),
    getCharacter(ECharacter.JOHN),
  ]);
}
function all (req, res) {
  //characterCollection[0].save()
  Character.find({type: 'JOHN'}).exec().then(characters => {
    res.json(characters)
  })
}

module.exports = {list, all};
