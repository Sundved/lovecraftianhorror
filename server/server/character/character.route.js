const express = require('express');
const characterCtrl = require('./character.controller');

const router = express.Router(); // eslint-disable-line new-cap

router.route('/')
  /** GET /api/characters - Get ACTIVE game */
  .get(
    characterCtrl.list
  )

router.route('/all')
  .get(
    characterCtrl.all
  )

module.exports = router;
