const {times} = require('lodash');
const Character = require('./character.model');
const {ECharacter} = require('./character.enum');
const {EArtifact} = require('../artifact/artifact.enum');
const {ECard} = require('../card/card.enum');
const {getArtifact} = require('../artifact/artifact.factory');
const {getCard} = require('../card/card.factory');

const getCharacter = (type) => {
  let cards;

  switch (type) {
    case ECharacter.JACK:
      cards = [
        ...times(6, () => getCard(ECard.ATTACK)),
        ...times(4, () => getCard(ECard.DEFENCE)),
      ];
      break;
    case ECharacter.KATE:
      cards = [
        ...times(5, () => getCard(ECard.ATTACK)),
        ...times(5, () => getCard(ECard.DEFENCE)),
      ];
      break;
    case ECharacter.JOHN:
      cards = [
        ...times(4, () => getCard(ECard.ATTACK)),
        ...times(6, () => getCard(ECard.DEFENCE)),
      ];
      break;
  }
  const character = new Character({
    type,
    cards,
  });
  character.save();

  return character;
};

module.exports = {getCharacter};
