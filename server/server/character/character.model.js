const mongoose = require('mongoose');
const {values} = require('lodash');
const {ECharacter} = require('./character.enum');

const CharacterSchema = new mongoose.Schema({
  type: {
    type: String,
    enum: values(ECharacter),
    required: true
  },
  artifacts: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Artifact',
  }],
  cards: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Card',
  }]
});

module.exports = mongoose.model('Character', CharacterSchema);
