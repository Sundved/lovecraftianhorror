const EAncient = {
  CTHULHU: 'CTHULHU',
  AZATHOTH: 'AZATHOTH',
  SHUB_NIGGURATH: 'SHUB_NIGGURATH',
};

module.exports = {EAncient};
