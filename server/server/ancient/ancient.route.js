const express = require('express');
const ancientCtrl = require('./ancient.controller');

const router = express.Router(); // eslint-disable-line new-cap

router.route('/')
  .get(
    ancientCtrl.list
  )

module.exports = router;
