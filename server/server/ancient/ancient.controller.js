const {getAncient} = require('./ancient.factory');
const {EAncient} = require('./ancient.enum');

function list(req, res) {
  res.json([
    getAncient(EAncient.AZATHOTH),
    getAncient(EAncient.CTHULHU),
    getAncient(EAncient.SHUB_NIGGURATH),
  ]);
}

module.exports = {list};
