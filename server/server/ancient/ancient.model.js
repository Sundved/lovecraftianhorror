const mongoose = require('mongoose');
const {values} = require('lodash');
const {EAncient} = require('./ancient.enum');

const AncientSchema = new mongoose.Schema({
  type: {
    type: String,
    enum: values(EAncient),
    required: true
  },
});

module.exports = mongoose.model('Ancient', AncientSchema);
