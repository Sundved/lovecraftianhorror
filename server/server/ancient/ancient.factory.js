const Ancient = require('./ancient.model');

const getAncient = (type) => {
  return new Ancient({
    type,
  });
};

module.exports = {getAncient};
