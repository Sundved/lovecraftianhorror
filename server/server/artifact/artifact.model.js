const mongoose = require('mongoose');
const {values} = require('lodash');
const {EArtifact} = require('./artifact.enum');
const {ERarity} = require('../common/common.enum');

const ArtifactSchema = new mongoose.Schema({
  type: {
    type: String,
    enum: values(EArtifact),
    required: true
  },
  rarity: {
    type: String,
    enum: values(ERarity),
    required: true,
    default: ERarity.COMMON
  },
});

module.exports = mongoose.model('Artifact', ArtifactSchema);
