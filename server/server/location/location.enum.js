const ELocation = {
  BATTLE: 'BATTLE',
  PORTAL: 'PORTAL',
};

const EPortal = {
  MONSTER: 'MONSTER'
}

module.exports = {ELocation, EPortal};
