const {sample} = require('lodash');
const {EPortal} = require('../location.enum');
const Portal = require('../models/portal.model');
const {getBattle} = require('../factory/battle.factory');

const getPortal = () => {
  const portalType = sample(EPortal);
  let event;

  switch (portalType) {
    case EPortal.MONSTER:
      event = getBattle();
      break;
  }

  return new Portal({
    portalType,
    event
  });
};

module.exports = {getPortal};
