const mongoose = require('mongoose');
const {values} = require('lodash');
const {ELocation} = require('../location.enum');

const PortalSchema = new mongoose.Schema({
  type: {
    type: String,
    enum: values(ELocation),
    required: true,
    default: ELocation.PORTAL
  },
  portalModel: {
    type: String,
    enum: ['Battle'],
    required: true
  },
  event: {
    type: mongoose.Schema.Types.ObjectId,
    refPath: 'portalModel',
    required: true
  },
});

module.exports = mongoose.model('Portal', PortalSchema);
