const mongoose = require('mongoose');
const {values} = require('lodash');
const {ELocation} = require('../location.enum');

const BattleSchema = new mongoose.Schema({
  type: {
    type: String,
    enum: values(ELocation),
    default: ELocation.BATTLE
  },
  monsters: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Monster',
  }],
});

module.exports = mongoose.model('Battle', BattleSchema);
