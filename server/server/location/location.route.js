const express = require('express');
const expressJwt = require('express-jwt');
const locationCtrl = require('./location.controller');
const config = require('../../config/config');

const router = express.Router(); // eslint-disable-line new-cap

router.route('/:type/:locationId')
/** GET /api/single - Get ACTIVE game */
  .get(
    expressJwt({ secret: config.jwtSecret }),
    locationCtrl.get
  )

module.exports = router;
