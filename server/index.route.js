const express = require('express');
const userRoutes = require('./server/user/user.route');
const authRoutes = require('./server/auth/auth.route');
const fileRoutes = require('./server/file/file.route');
const singleRoutes = require('./server/single/single.route');
const characterRoutes = require('./server/character/character.route');
const ancientRoutes = require('./server/ancient/ancient.route');

const router = express.Router(); // eslint-disable-line new-cap

// TODO: use glob to match *.route files

/** GET /health-check - Check service health */
router.get('/health-check', (req, res) =>
  res.send('OK')
);

// mount user routes at /users
router.use('/users', userRoutes);

// mount auth routes at /auth
router.use('/auth', authRoutes);

router.use('/files', fileRoutes);

router.use('/single', singleRoutes);

router.use('/characters', characterRoutes);

router.use('/ancients', ancientRoutes);

module.exports = router;
