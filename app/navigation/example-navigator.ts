import { createStackNavigator } from "react-navigation"
import { LoginScreen } from "../screens/login";
import {MainScreen} from '../screens/main';
import { SecondExampleScreen } from "../screens/second-example-screen";
import {SplashScreen} from '../screens/splash';
import {ProfileScreen} from '../screens/profile';
import {CharactersScreen} from '../screens/characters';
import {AncientsScreen} from '../screens/ancients';
import {RoadMapScreen} from '../screens/road-map';

export const ExampleNavigator = createStackNavigator({
    Splash: {
        screen: SplashScreen,
        navigationOptions: {
            gesturesEnabled: false,
        }
    },
    Login: {
        screen: LoginScreen,
        navigationOptions: {
            gesturesEnabled: false,
        }
    },
    Main: {
        screen: MainScreen,
        navigationOptions: {
            gesturesEnabled: false,
        }
    },
    Profile: {screen: ProfileScreen},
    Characters: {screen: CharactersScreen},
    Ancients: {screen: AncientsScreen},
    RoadMap: {screen: RoadMapScreen},
    secondExample: { screen: SecondExampleScreen },
},
{
  headerMode: "none",
})
