import {IAnyType, IArrayType, SnapshotOut, types} from 'mobx-state-tree';
import {EStatusType} from '../services/api/api.enums';
import {ApiResponse} from 'apisauce';
import {withStatus} from './extensions';
import {withErrors} from './extensions/with-errors';

export const getAsyncModel = <T extends IAnyType | IArrayType<IAnyType>>(model: T) => {
    return types.model('Async').props({
        data: types.maybeNull(model),
    }).extend(withStatus).extend(withErrors)
        .actions(self => ({
            setData (value: SnapshotOut<T>) {
                self.data = value;
                self.setStatus(EStatusType.DONE);
            }
        }))
        .actions(self => ({
            asyncRequest: async (request: Promise<ApiResponse<SnapshotOut<T>>>): Promise<ApiResponse<SnapshotOut<T>>> => {
                self.setStatus(EStatusType.PENDING);

                const res = await request;
                if (res.ok) {
                    self.setData(res.data);
                } else if (res.ok === false) {
                    self.setErrors(res);
                    self.setStatus(EStatusType.ERROR);
                }
                return res;
            },
    }));
};
