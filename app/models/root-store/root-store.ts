import { Instance, SnapshotOut, types } from "mobx-state-tree"
import { NavigationStoreModel } from "../../navigation/navigation-store";
import {UserStoreModel} from '../user-store';
import {SingleStoreModel} from '../single-store';

/**
 * An RootStore model.
 */
export const RootStoreModel = types.model("RootStore").props({
    navigationStore: types.optional(NavigationStoreModel, {}),
    userStore: types.optional(UserStoreModel, {
        user: {data: null},
        auth: {data: null}
    }),
    singleStore: types.optional(SingleStoreModel, {
        single: {data: null},
        characters: {data: []},
        selectedCharacter: null,
        ancients: {data: []},
        selectedAncient: null,
    }),
});

/**
 * The RootStore instance.
 */
export type IRootStore = Instance<typeof RootStoreModel>

/**
 * The data of an RootStore.
 */
export type IRootStoreSnapshot = SnapshotOut<typeof RootStoreModel>
