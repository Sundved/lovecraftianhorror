import {IObservableValue, observable} from 'mobx';
import {ApiErrorResponse} from 'apisauce';
import {IStateTreeNode} from 'mobx-state-tree';
import {getGeneralApiProblem} from '../../services/api/api-problem';

export const withErrors = (self: IStateTreeNode) => {
    let errors: IObservableValue<ApiErrorResponse<any>> = observable.box(null);

    return {
        views: {
            // a getter
            get errors() {
                return errors.get();
            },
            // as setter
            set errors(value: ApiErrorResponse<any>) {
                errors.set(value)
            },
            get problem() {
                const error = errors.get();
                const problem = getGeneralApiProblem(error);

                return problem && problem.kind;
            }
        },
        actions: {
            /**
             * Set the errors to something new.
             *
             * @param value The new errors.
             */
            setErrors(value: ApiErrorResponse<any>) {
                errors.set(value)
            }
        }
    }
}
