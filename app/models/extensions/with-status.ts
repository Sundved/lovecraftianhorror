import {IStateTreeNode} from 'mobx-state-tree';
import {IObservableValue, observable} from 'mobx';
import {EStatusType} from '../../services/api/api.enums';

/**
 * Adds a status field to the model often for tracking api access.
 *
 * This property is a string which can be observed, but will not
 * participate in any serialization.
 *
 * Use this to extend your models:
 *
 * ```ts
 *   types.model('MyModel')
 *     .props({})
 *     .actions(self => ({}))
 *     .extend(withStatus) // <--- time to shine baby!!!
 * ```
 *
 * This will give you these 3 options:
 *
 *   .status            // returns a string
 *   .status = 'done'   // change the status directly
 *   .setStatus('done') // change the status and trigger an mst action
 */
export const withStatus = (self: IStateTreeNode) => {
    /**
     * The observable backing store for the status field.
     */
    let status: IObservableValue<string> = observable.box(EStatusType.IDLE);

    return {
        views: {
            // a getter
            get status() {
                return status.get() as EStatusType
            },
            // as setter
            set status(value: EStatusType) {
                status.set(value)
            },
        },
        actions: {
            /**
             * Set the status to something new.
             *
             * @param value The new status.
             */
            setStatus(value: EStatusType) {
                status.set(value)
            }
        }
    }
};
