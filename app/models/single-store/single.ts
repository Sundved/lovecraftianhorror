import {Instance, SnapshotOut, types} from 'mobx-state-tree';
import {LevelModel} from './level';
import {values} from 'lodash';
import {ESingleGameStatus} from '../../services/api/api.enums';

export const SingleModel = types.model('Single').props({
    _id: types.identifier,
    userId: types.string,
    createdAt: types.string,
    status: types.enumeration(values(ESingleGameStatus)),
    levels: types.array(LevelModel)
});

export type ISingle = Instance<typeof SingleModel>;
export type ISingleSnapshot = SnapshotOut<typeof SingleModel>;
