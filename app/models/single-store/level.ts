import {Instance, SnapshotOut, types} from 'mobx-state-tree';
import {LinkModel} from './link';

export const LevelModel = types.model('Level').props({
    number: types.number,
    links: types.array(LinkModel)
});

export type ILevel = Instance<typeof LevelModel>;
export type ILevelSnapshot = SnapshotOut<typeof LevelModel>;
