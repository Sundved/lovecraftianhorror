import {Instance, SnapshotOut, types} from 'mobx-state-tree';
import {values} from 'lodash';
import {ELinkDirection} from '../../services/api/api.enums';

export const DirectionModel = types.model('Direction').props({
    direction: types.enumeration<ELinkDirection>(values(ELinkDirection)),
    linkId: types.string
});

export type IDirection = Instance<typeof DirectionModel>;
export type IDirectionSnapshot = SnapshotOut<typeof DirectionModel>;
