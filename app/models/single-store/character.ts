import {values} from 'lodash';
import {Instance, SnapshotOut, types} from 'mobx-state-tree';
import {ECharacter} from '../../services/api/api.enums';

export const CharacterModel = types.model('Character').props({
    type: types.enumeration(values(ECharacter)),
});

export type ICharacter = Instance<typeof CharacterModel>;
export type ICharacterSnapshot = SnapshotOut<typeof CharacterModel>;
