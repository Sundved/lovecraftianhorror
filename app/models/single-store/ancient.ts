import {values} from 'lodash';
import {Instance, SnapshotOut, types} from 'mobx-state-tree';
import {EAncient} from '../../services/api/api.enums';

export const AncientModel = types.model('Ancient').props({
    type: types.enumeration(values(EAncient)),
});

export type IAncient = Instance<typeof AncientModel>;
export type IAncientSnapshot = SnapshotOut<typeof AncientModel>;
