import {Instance, SnapshotOut, types} from 'mobx-state-tree';
import {DirectionModel} from './direction';

export const LinkModel = types.model('Link').props({
    directions: types.array(DirectionModel),
});

export type ILink = Instance<typeof LinkModel>;
export type ILinkSnapshot = SnapshotOut<typeof LinkModel>;
