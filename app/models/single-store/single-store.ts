import {values} from 'lodash';
import {IArrayType, Instance, SnapshotOut, types} from 'mobx-state-tree';
import {withEnvironment} from '../extensions';
import {getAsyncModel} from '../async-store';
import {SingleModel, ISingleSnapshot} from './single';
import {CharacterModel, ICharacterSnapshot} from './character';
import {EAncient, ECharacter} from '../../services/api/api.enums';
import {AncientModel, IAncientSnapshot} from './ancient';
import {ApiResponse} from 'apisauce';

export const SingleStoreModel = types.model('SingleStore').props({
    single: getAsyncModel<typeof SingleModel>(SingleModel),
    characters: getAsyncModel<IArrayType<typeof CharacterModel>>(types.array(CharacterModel)),
    selectedCharacter: types.maybeNull(types.enumeration<ECharacter>(values(ECharacter))),
    ancients: getAsyncModel<IArrayType<typeof AncientModel>>(types.array(AncientModel)),
    selectedAncient: types.maybeNull(types.enumeration<EAncient>(values(EAncient))),
}).extend(withEnvironment).actions(self => ({
    newSingleGame: async (): Promise<ApiResponse<ISingleSnapshot>> => {
        const res = await self.single.asyncRequest(
            self.environment.api.newSingleGame(self.selectedCharacter, self.selectedAncient)
        );
        if (res.ok) {
            self.single.setData(res.data);
        } else {
            throw res;
        }
        return res;
    },
    loadCharacters: async (): Promise<ApiResponse<ICharacterSnapshot[]>> => {
        const res = await self.characters.asyncRequest(self.environment.api.loadCharacters());
        if (res.ok) {
            self.characters.setData(res.data);
        } else {
            throw res;
        }
        return res;
    },
    selectCharacter (character: ECharacter) {
        self.selectedCharacter = character;
    },
    loadAncients: async (): Promise<ApiResponse<IAncientSnapshot[]>> => {
        const res = await self.ancients.asyncRequest(self.environment.api.loadAncients());
        if (res.ok) {
            self.ancients.setData(res.data);
        } else {
            throw res;
        }
        return res;
    },
    selectAncient (ancient: EAncient) {
        self.selectedAncient = ancient;
    },
    loadSingleGame: async (singleId: string): Promise<ApiResponse<ISingleSnapshot>> => {
        const res = await self.single.asyncRequest(
            self.environment.api.loadSingleGame(singleId)
        );
        if (res.ok) {
            self.single.setData(res.data);
        } else {
            throw res;
        }
        return res;
    },
    breakSingleGame: async (): Promise<ApiResponse<void>> => {
        const res = await self.environment.api.breakSingleGame();
        if (!res.ok) {
            throw res;
        }
        return res;
    },
}));

export type ISingleStore = Instance<typeof SingleStoreModel>;
export type ISingleStoreSnapshot = SnapshotOut<typeof SingleStoreModel>;
