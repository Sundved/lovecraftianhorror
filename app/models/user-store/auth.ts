import {Instance, SnapshotOut, types} from 'mobx-state-tree';
import * as storage from '../../utils/storage';

/**
 * An RootStore model.
 */
export const AuthModel = types.model('Auth').props({
    token: types.maybeNull(types.string),
}).actions(self => ({
    saveToken: async (token) => {
        await storage.saveString('AUTH_TOKEN', token);
    },
    setToken (token) {
        self.token = token;
    },
}));

export type IAuth = Instance<typeof AuthModel>;
export type IAuthSnapshot = SnapshotOut<typeof AuthModel>;
