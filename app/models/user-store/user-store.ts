import {getRoot, Instance, SnapshotOut, types} from 'mobx-state-tree';
import {UserModel} from './user';
import {IRootStore} from '../root-store';
import {AuthModel} from './auth';
import {withEnvironment} from '../extensions';
import {getAsyncModel} from '../async-store';
import * as storage from '../../utils/storage';
import {ImagePickerResponse} from 'react-native-image-picker';

/**
 * An RootStore model.
 */
export const UserStoreModel = types.model('UserStore').props({
    auth: getAsyncModel<typeof AuthModel>(AuthModel),
    user: getAsyncModel<typeof UserModel>(UserModel),
}).extend(withEnvironment).actions(self => ({
    getAuthToken: async () => {
        const token = await storage.loadString('AUTH_TOKEN');
        if (token) {
            self.auth.setData({token});
        } else {
            getRoot<IRootStore>(self).navigationStore.navigateTo('Login');
        }
    },
})).actions(self => ({
    setupAuth: async () => {
        //storage.clear();
        await self.getAuthToken();
        if (self.auth.data.token) {
            self.environment.api.setAuthToken(self.auth.data.token);

            const res = await self.user.asyncRequest(self.environment.api.getUserInfo());
            if (res.ok) {
                getRoot<IRootStore>(self).navigationStore.navigateTo('Main');
            }
        }
    }
})).actions(self => ({
    loginUser: async (username: string, password: string) => {
        const res = await self.environment.api.loginUser(username, password);
        if (res.ok) {
            self.auth.setData({token: res.data.token});
            await self.auth.data.saveToken(res.data.token);
            getRoot<IRootStore>(self).navigationStore.replace('Splash');
            await self.setupAuth();
        } else if (res.ok === false) {
            self.auth.setErrors(res);
        }
    },
    uploadAvatar: async (data: ImagePickerResponse) => {
        const res = await self.environment.api.uploadAvatar(data);
        if (res.ok) {
            self.user.data.setAvatar(res.data.avatar);
        } else if (res.ok === false) {
            self.user.setErrors(res);
        }
    }
}));

export type IUserStore = Instance<typeof UserStoreModel>;
export type IUserStoreSnapshot = SnapshotOut<typeof UserStoreModel>;
