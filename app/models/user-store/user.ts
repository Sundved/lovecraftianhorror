import {Instance, SnapshotOut, types} from 'mobx-state-tree';

/**
 * An RootStore model.
 */
export const UserModel = types.model('User').props({
    _id: types.identifier,
    username: types.string,
    createdAt: types.string,
    avatar: types.maybeNull(types.string),
    singleId: types.maybeNull(types.string),
}).actions(self => ({
    setAvatar: (value: string) => {
        self.avatar = value;
    },
    breakSingleGame: () => {
        self.singleId = null;
    }
}));

export type IUser = Instance<typeof UserModel>;
export type IUserSnapshot = SnapshotOut<typeof UserModel>;
