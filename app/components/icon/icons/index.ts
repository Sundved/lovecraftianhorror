export const icons = {
    back: require("./arrow-left.png"),
    bullet: require("./bullet.png"),
    user: require('./user-circle-regular.svg'),
    pencil: require('./pencil-alt-solid.svg'),
}

export type IconTypes = keyof typeof icons
