import * as React from "react"
import { View, Image, ImageStyle } from "react-native"
import { IconProps } from "./icon.props"
import { icons } from "./icons"
import SvgUri from 'react-native-svg-uri';

const ROOT: ImageStyle = {
  resizeMode: "contain",
}

export function Icon(props: IconProps) {
  const { style: styleOverride, icon, containerStyle } = props
  const style: ImageStyle = { ...ROOT, ...styleOverride }

  return (
    <View style={containerStyle}>
      <Image style={style} source={icons[icon]} />
    </View>
  )
}

export interface IconSvgStyle extends ImageStyle {
    fill: string;
}
export interface IconSvgProps extends IconProps {
    style?: IconSvgStyle;
}

export function IconSvg(props: IconSvgProps) {
    const {icon, containerStyle, style } = props;

    return (
        <View style={containerStyle}>
            <SvgUri
                {...style}
                source={icons[icon]}
            />
        </View>
    )
}
