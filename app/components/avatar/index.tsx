import * as React from "react"
import {ImageStyle, View, ViewStyle, Image} from "react-native"
import {IconSvg} from '../icon';
import {Button} from '../button';
import ImagePicker, {ImagePickerResponse} from 'react-native-image-picker';
import {DEFAULT_API_CONFIG} from '../../services/api/api-config';

const options = {
    title: 'Select Avatar',
    customButtons: [{ name: 'fb', title: 'Choose Photo from Facebook' }],
    storageOptions: {
        skipBackup: true,
        path: 'images',
    },
};
const EDIT_CONTAINER: ViewStyle = {
    position: 'absolute',
    bottom: 0,
    right: 0,
    padding: 10,
    backgroundColor: 'gray'
};

export interface AvatarProps extends ImageStyle {
    width: number;
    height: number;
    source: string;
    containerStyle?: ViewStyle;
    editable?: boolean;
    onSetAvatar?: (data: ImagePickerResponse) => void;
}

export class Avatar extends React.Component<AvatarProps, {}>{
    state = {};
    handleEditAvatarPress = () => {
        ImagePicker.showImagePicker(options, (response: ImagePickerResponse) => {
            console.log('Response = ', response);

            if (response.didCancel) {
                console.tron.log('User cancelled image picker');
            } else if (response.error) {
                console.tron.log('ImagePicker Error: ', response.error);
            } else if (response.customButton) {
                console.tron.log('User tapped custom button: ', response.customButton);
            } else {
                console.tron.log(response.uri);
                // You can also display the image using data:
                // const source = { uri: 'data:image/jpeg;base64,' + response.data };

                this.props.onSetAvatar(response);
            }
        });
    };

    renderEditIcon () {
        const {editable, width, height} = this.props;
        return editable ? (
            <Button preset="link" onPress={this.handleEditAvatarPress}>
                <IconSvg
                    icon="pencil"
                    style={{
                        width: width/5,
                        height: height/5,
                        fill: 'white'
                    }}
                    containerStyle={{
                        ...EDIT_CONTAINER,
                        borderRadius: (width/5 + 20)/2
                    }}
                />
            </Button>
        ): null;
    }

    render () {
        const {source, containerStyle, width, height} = this.props;

        return (
            <View style={containerStyle}>
                {source ? (
                    <View style={{alignSelf: 'flex-start'}}>
                        <Image source={{uri: DEFAULT_API_CONFIG.url + source}} style={{width, height, borderRadius: width/2}}/>
                        {this.renderEditIcon()}
                    </View>
                ) : (
                    <View style={{alignSelf: 'flex-start'}}>
                        <IconSvg
                            icon="user"
                            style={{
                                width,
                                height,
                                fill: 'white'
                            }}
                        />
                        {this.renderEditIcon()}
                    </View>
                )}

            </View>
        );
    }
}
