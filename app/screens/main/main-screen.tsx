import * as React from 'react';
import {includes} from 'lodash';
import {ImageStyle, TextStyle, TouchableOpacity, View, ViewStyle} from 'react-native'
import {NavigationScreenProps} from 'react-navigation'
import {Text} from '../../components/text'
import {Wallpaper} from '../../components/wallpaper'
import {Header} from '../../components/header'
import {color, spacing} from '../../theme'
import {inject, observer} from 'mobx-react';
import {ISingleStore} from '../../models/single-store';
import {EStatusType} from '../../services/api/api.enums';
import {IUser, IUserStore} from '../../models/user-store';
import {Button} from '../../components/button';
import {IRootStore} from '../../models/root-store';

const FULL: ViewStyle = { flex: 1 }
const CONTAINER: ViewStyle = {
  backgroundColor: color.transparent,
  paddingHorizontal: spacing[4],
}
const TEXT: TextStyle = {
  color: color.palette.white,
  fontFamily: 'Montserrat',
}
const BOLD: TextStyle = { fontWeight: 'bold' }
const HEADER: TextStyle = {
  paddingTop: spacing[3],
  paddingBottom: spacing[4] + spacing[1],
  paddingHorizontal: 0,
}
const HEADER_TITLE: TextStyle = {
  ...TEXT,
  ...BOLD,
  fontSize: 12,
  lineHeight: 15,
  textAlign: 'center',
  letterSpacing: 1.5,
}
const TITLE_WRAPPER: TextStyle = {
  ...TEXT,
  textAlign: 'center',
}
const TITLE: TextStyle = {
  ...TEXT,
  ...BOLD,
  fontSize: 28,
  lineHeight: 38,
  textAlign: 'center',
}
const ALMOST: TextStyle = {
  ...TEXT,
  ...BOLD,
  fontSize: 26,
  fontStyle: 'italic',
}
const BOWSER: ImageStyle = {
  alignSelf: 'center',
  marginVertical: spacing[5],
  maxWidth: '100%',
}
const CONTENT: TextStyle = {
  ...TEXT,
  color: '#BAB6C8',
  fontSize: 15,
  lineHeight: 22,
  marginBottom: spacing[5],
}

export interface MainScreenProps extends NavigationScreenProps<{}> {
    user: IUser;
    singleStore: ISingleStore;
}

@inject((stores: IRootStore) => ({
    user: stores.userStore.user.data,
    singleStore: stores.singleStore
}))
@observer
export class MainScreen extends React.Component<MainScreenProps, {}> {
    handleProfilePress = () => this.props.navigation.navigate('Profile');
    handleNewGamePress = async () => {
        const {singleStore} = this.props;
        if (includes([EStatusType.IDLE, EStatusType.ERROR], singleStore.characters.status)) {
            singleStore.loadCharacters().then(() => {
                this.props.navigation.navigate('Characters');
            });
        }
    };
    handleContinue = () => {
        const {singleStore, user: {singleId}} = this.props;

        singleStore.loadSingleGame(singleId).then(() => {
            this.props.navigation.navigate('RoadMap');
        });
    };
    handleBreak = () => {
        const {singleStore, user} = this.props;

        singleStore.breakSingleGame();
        user.breakSingleGame();
    };

    render() {
        const {user} = this.props;

        return (
            <View style={FULL}>
                <Wallpaper/>
                <Header
                    style={HEADER}
                    titleStyle={HEADER_TITLE}
                    rightIcon="user"
                    onRightPress={this.handleProfilePress}
                />
                <View
                  style={{flex: 1, flexDirection: 'row'}}
                >
                    {!user.singleId ? (
                        <TouchableOpacity
                            style={{
                                display: 'flex',
                                flexGrow: 1,
                                backgroundColor: 'red',
                                flexDirection: 'column'
                            }}
                            onPress={this.handleNewGamePress}
                        >
                            <View style={{flex: 1, justifyContent: 'center'}}>
                                <Text style={{textAlign: 'center'}} text="New Game"/>
                            </View>
                        </TouchableOpacity>
                        ) : (
                            <TouchableOpacity
                                style={{
                                    display: 'flex',
                                    flexGrow: 1,
                                    backgroundColor: 'red',
                                    flexDirection: 'column'
                                }}
                                onPress={this.handleContinue}
                            >
                                <View
                                    style={{
                                        flexGrow: 1
                                    }}
                                >
                                    <Text style={{
                                        textAlign: 'center'
                                    }} text="Continue"/>

                                </View>
                                <Button
                                    style={{
                                        paddingVertical: spacing[4],
                                        paddingHorizontal: spacing[4],
                                        backgroundColor: "#5D2555",
                                        flexShrink: 0
                                    }}
                                    text="Break"
                                    onPress={this.handleBreak}
                                />
                            </TouchableOpacity>
                    )}
                    <View style={{flex: 1, backgroundColor: 'green'}}/>
                    <View style={{flex: 1, backgroundColor: 'blue'}}/>
                </View>
            </View>
        )
    }
}
