import * as React from "react"
import {Image, ImageStyle, TextStyle, View, ViewStyle} from "react-native"
import {NavigationScreenProps} from "react-navigation"
import {Screen} from "../../components/screen"
import {Wallpaper} from "../../components/wallpaper"
import {color, spacing} from "../../theme"
import {bowserLogo} from "./";
import {inject, observer} from 'mobx-react';
import {IUserStore} from '../../models/user-store';
import {Text} from '../../components/text';

const FULL: ViewStyle = { flex: 1 };
const CONTAINER: ViewStyle = {
  backgroundColor: color.transparent,
  paddingHorizontal: spacing[4],
};
const BOWSER: ImageStyle = {
    alignSelf: "center",
    marginVertical: spacing[5],
    maxWidth: "100%",
};
const TEXT: TextStyle = {
    color: color.palette.white,
    fontFamily: "Montserrat",
}
const BOLD: TextStyle = { fontWeight: "bold" }
const TITLE: TextStyle = {
    ...TEXT,
    ...BOLD,
    fontSize: 28,
    lineHeight: 38,
    textAlign: "center",
}

export interface SplashScreenProps extends NavigationScreenProps<{}> {
    userStore: IUserStore;
}

@inject('userStore')
@observer
export class SplashScreen extends React.Component<SplashScreenProps, {}> {
    componentDidMount() {
        console.tron.log('Try get auth token');
        this.props.userStore.setupAuth();
    }

    render() {
        const {userStore} = this.props;

        return (
            <View style={FULL}>
                <Wallpaper/>
                <Screen
                    style={CONTAINER}
                    preset="scroll"
                    backgroundColor={color.transparent}>
                    <Image source={bowserLogo} style={BOWSER}/>
                    {userStore.user.problem && (
                        <Text style={TITLE} preset="header" tx={`errors.${userStore.user.problem}`}/>
                    )}
                </Screen>
            </View>
        )
    }
}
