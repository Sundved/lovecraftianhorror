import {includes, isEmpty} from 'lodash';
import * as React from 'react';
import {View, ViewStyle} from 'react-native'
import {NavigationScreenProps} from 'react-navigation'
import {Wallpaper} from '../../components/wallpaper'
import {Ancient} from './ancient';
import {inject, observer} from 'mobx-react';
import {ISingleStore} from '../../models/single-store';
import {EAncient, EStatusType} from '../../services/api/api.enums';

const FULL: ViewStyle = { flex: 1 }

export interface AncientScreenProps extends NavigationScreenProps<{}> {
    singleStore: ISingleStore;
}

interface IState {
    selectedAncient: EAncient;
}

@inject('singleStore')
@observer
export class AncientsScreen extends React.Component<AncientScreenProps, IState> {
    state: IState = {
        selectedAncient: null,
    };

    handleSelect = (selectedAncient: EAncient) => {
        this.setState({selectedAncient});
    };

    handleContinue = (selectedAncient: EAncient) => {
        const {singleStore} = this.props;

        singleStore.selectAncient(selectedAncient);
        singleStore.newSingleGame().then(() => {
            this.props.navigation.navigate('RoadMap');
        });
    };

    render() {
        const {singleStore: {ancients}} = this.props;
        const {selectedAncient} = this.state;

        return (
            <View style={FULL}>
                <Wallpaper/>
                <View
                  style={{flex: 1, flexDirection: 'row'}}
                >
                    {!isEmpty(ancients.data) ? ancients.data.map(ancient => (
                        <Ancient
                            key={ancient.type}
                            name={ancient.type}
                            isSelected={selectedAncient === ancient.type}
                            onSelect={this.handleSelect}
                            onContinue={this.handleContinue}
                        />
                    )): null}
                </View>
            </View>
        )
    }
}
