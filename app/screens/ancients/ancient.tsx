import * as React from 'react'
import {Animated, TouchableOpacity, View} from 'react-native'
import {Text} from '../../components/text'
import {spacing} from '../../theme';
import {Screen} from '../../components/screen';
import {Button} from '../../components/button';
import {EAncient} from '../../services/api/api.enums';

export interface IAncientProps {
    name: EAncient;
    isSelected: boolean;
    onSelect: (name: EAncient) => void;
    onContinue:(name: EAncient) => void;
}

interface IState {
    containerWidth: Animated.Value;
    ancientWidth: Animated.Value;
    descriptionWidth: Animated.Value;
}

export class Ancient extends React.Component<IAncientProps, IState> {
    state: IState = {
        containerWidth: new Animated.Value(0),
        descriptionWidth: new Animated.Value(0),
        ancientWidth: new Animated.Value(100),
    };

    componentDidUpdate (prevProps: IAncientProps) {
        if (this.props.isSelected !== prevProps.isSelected) {
            Animated.timing(this.state.containerWidth, {
                toValue: this.props.isSelected ? 80 : 0,
                duration: 1000
            }).start();
            Animated.timing(this.state.descriptionWidth, {
                toValue: this.props.isSelected ? 65 : 0,
                duration: 1000
            }).start();
            Animated.timing(this.state.ancientWidth, {
                toValue: this.props.isSelected ? 35 : 100,
                duration: 1000
            }).start();
        }
    }

    handleSelect = () => {
        this.props.onSelect(this.props.name);
    };

    handleContinue = () => {
        this.props.onContinue(this.props.name);
    };


    render() {
        const {name} = this.props;
        const {containerWidth, descriptionWidth, ancientWidth} = this.state;
        let color = null;
        switch (name) {
            case EAncient.AZATHOTH:
                color = 'red';
                break;
            case EAncient.CTHULHU:
                color = 'green';
                break;
            case EAncient.SHUB_NIGGURATH:
                color = 'blue';
                break;
        }

        return (
            <Animated.View
                style={{
                    flex: 1,
                    backgroundColor: color,
                    flexBasis: containerWidth.interpolate({
                        inputRange: [0, 100],
                        outputRange: ['0%', '100%'],
                    }),
                    flexDirection: 'row',
                }}
            >
                <Animated.View
                    style={{
                        width: ancientWidth.interpolate({
                            inputRange: [0, 100],
                            outputRange: ['0%', '100%'],
                        })
                    }}
                >
                    <TouchableOpacity
                        onPress={this.handleSelect}
                        style={{flex: 1, flexDirection: 'column'}}
                    >
                        <View style={{flex: 1, justifyContent: 'flex-end', paddingBottom: 20}}>
                            <Text style={{textAlign: 'center'}} text={name}/>
                        </View>
                    </TouchableOpacity>
                </Animated.View>

                <Animated.View
                    style={{
                        width: descriptionWidth.interpolate({
                            inputRange: [0, 100],
                            outputRange: ['0%', '100%'],
                        }),
                        backgroundColor: 'black'
                    }}
                >
                    <Screen
                        style={{
                            minWidth: 300
                        }}
                        preset="scroll"
                        backgroundColor={color.transparent}
                    >

                    </Screen>
                    <Button
                        style={{
                            paddingVertical: spacing[4],
                            paddingHorizontal: spacing[4],
                            backgroundColor: "#5D2555",
                            minWidth: 300,
                        }}
                        tx="firstExampleScreen.continue"
                        onPress={this.handleContinue}
                    />
                </Animated.View>
            </Animated.View>

        )
    }
}
