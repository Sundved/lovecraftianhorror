import * as React from "react"
import {Image, ImageStyle, TextStyle, View, ViewStyle} from "react-native"
import {NavigationScreenProps} from "react-navigation"
import {Text} from "../../components/text"
import {Button} from "../../components/button"
import {Screen} from "../../components/screen"
import {Wallpaper} from "../../components/wallpaper"
import {Header} from "../../components/header"
import {color, spacing} from "../../theme"
import {bowserLogo} from "./"
import {TextField} from '../../components/text-field';
import {inject, observer} from 'mobx-react';
import {IUserStore} from '../../models/user-store';

const FULL: ViewStyle = { flex: 1 }
const CONTAINER: ViewStyle = {
  backgroundColor: color.transparent,
  paddingHorizontal: spacing[4],
}
const TEXT: TextStyle = {
  color: color.palette.white,
  fontFamily: "Montserrat",
}
const BOLD: TextStyle = { fontWeight: "bold" }
const HEADER: TextStyle = {
  paddingTop: spacing[3],
  paddingBottom: spacing[4] + spacing[1],
  paddingHorizontal: 0,
}
const HEADER_TITLE: TextStyle = {
  ...TEXT,
  ...BOLD,
  fontSize: 12,
  lineHeight: 15,
  textAlign: "center",
  letterSpacing: 1.5,
}
const TITLE: TextStyle = {
  ...TEXT,
  ...BOLD,
  fontSize: 28,
  lineHeight: 38,
  textAlign: "center",
}
const BOWSER: ImageStyle = {
  alignSelf: "center",
  marginVertical: spacing[5],
  maxWidth: "100%",
}
const CONTINUE: ViewStyle = {
  paddingVertical: spacing[4],
  paddingHorizontal: spacing[4],
  backgroundColor: "#5D2555",
};
const CONTINUE_TEXT: TextStyle = {
  ...TEXT,
  ...BOLD,
  fontSize: 13,
  letterSpacing: 2,
};

export interface IProps extends NavigationScreenProps<{}> {
    userStore: IUserStore;
}
interface IState {
    username: string;
    password: string;
}

@inject('userStore')
@observer
export class LoginScreen extends React.Component<IProps, IState> {
    state: IState = {
        username: '',
        password: ''
    };
    handleLogin = () => {
        const {username, password} = this.state;
        this.props.userStore.loginUser(username, password);
    };

    handleUsernameChange = (username) => {
        this.setState({username});
    };

    handlePasswordChange = (password) => {
        this.setState({password});
    };

    render() {
        const {userStore} = this.props;
        const {username, password} = this.state;

        return (
            <View style={FULL}>
                <Wallpaper/>
                <Screen
                    style={CONTAINER}
                    preset="scroll"
                    backgroundColor={color.transparent}>
                    <Header
                        headerTx="firstExampleScreen.poweredBy"
                        style={HEADER}
                        titleStyle={HEADER_TITLE}
                    />
                    <Text style={TITLE} preset="header" tx="firstExampleScreen.readyForLaunch"/>
                    <Image source={bowserLogo} style={BOWSER}/>
                    <TextField
                        label="Login"
                        onChangeText={this.handleUsernameChange}
                        value={username}
                    />
                    <TextField
                        label="Password"
                        onChangeText={this.handlePasswordChange}
                        secureTextEntry
                        value={password}
                    />
                    <Text style={TITLE} preset="header" tx={`errors.${userStore.auth.problem}`}/>
                </Screen>
                <Button
                    style={CONTINUE}
                    textStyle={CONTINUE_TEXT}
                    tx="firstExampleScreen.continue"
                    onPress={this.handleLogin}
                />
            </View>
        )
    }
}
