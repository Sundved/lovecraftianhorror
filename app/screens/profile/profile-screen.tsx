import * as React from "react"
import {Image, ImageStyle, TextStyle, View, ViewStyle} from "react-native"
import {NavigationScreenProps} from "react-navigation"
import {Screen} from "../../components/screen"
import {Wallpaper} from "../../components/wallpaper"
import {color, spacing} from "../../theme"
import {inject, observer} from 'mobx-react';
import {IUserStore} from '../../models/user-store';
import {Text} from '../../components/text';
import {Icon, IconSvg, IconSvgProps, IconSvgStyle} from '../../components/icon';
import {Button} from '../../components/button';
import {Avatar} from '../../components/avatar';
import {ImagePickerResponse} from 'react-native-image-picker';

const FULL: ViewStyle = { flex: 1 };
const CONTAINER: ViewStyle = {
  backgroundColor: color.transparent,
  paddingHorizontal: spacing[4],
};
const BOWSER: ImageStyle = {
    alignSelf: "center",
    marginVertical: spacing[5],
    maxWidth: "100%",
};
const TEXT: TextStyle = {
    color: color.palette.white,
    fontFamily: "Montserrat",
}
const BOLD: TextStyle = { fontWeight: "bold" }
const USER: IconSvgStyle = {
    width: 150,
    height: 150,
    fill: 'white',
};
const USER_CONTAINER: ViewStyle = {
    alignItems: 'center'
};

export interface ProfileScreen extends NavigationScreenProps<{}> {
    userStore: IUserStore;
}

@inject('userStore')
@observer
export class ProfileScreen extends React.Component<ProfileScreen, {}> {
    handleSetAvatar = (data: ImagePickerResponse) => {
        this.props.userStore.uploadAvatar(data);
    };

    render() {
        const {userStore} = this.props;

        return (
            <View style={FULL}>
                <Wallpaper/>
                <Screen
                    style={CONTAINER}
                    preset="scroll"
                    backgroundColor={color.transparent}
                >
                    <Avatar
                        containerStyle={{alignSelf: 'center'}}
                        width={150}
                        height={150}
                        editable
                        source={userStore.user.data && userStore.user.data.avatar}
                        onSetAvatar={this.handleSetAvatar}
                    />
                </Screen>
            </View>
        )
    }
}
