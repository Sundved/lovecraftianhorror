import {includes, isEmpty} from 'lodash';
import * as React from 'react';
import {View, ViewStyle} from 'react-native'
import {NavigationScreenProps} from 'react-navigation'
import {Wallpaper} from '../../components/wallpaper'
import {Character} from './character';
import {inject, observer} from 'mobx-react';
import {ISingleStore} from '../../models/single-store';
import {ECharacter, EStatusType} from '../../services/api/api.enums';

const FULL: ViewStyle = { flex: 1 }

export interface CharactersScreenProps extends NavigationScreenProps<{}> {
    singleStore: ISingleStore;
}

interface IState {
    selectedCharacter: ECharacter;
}

@inject('singleStore')
@observer
export class CharactersScreen extends React.Component<CharactersScreenProps, IState> {
    state: IState = {
        selectedCharacter: null,
    };

    handleSelectCharacter = (selectedCharacter: ECharacter) => {
        this.setState({selectedCharacter});
    };

    handleContinue = (selectedCharacter: ECharacter) => {
        const {singleStore} = this.props;

        singleStore.selectCharacter(selectedCharacter);
        if (includes([EStatusType.IDLE, EStatusType.ERROR], singleStore.ancients.status)) {
            singleStore.loadAncients().then(() => {
                this.props.navigation.navigate('Ancients');
            });
        }
    };

    render() {
        const {singleStore: {characters}} = this.props;
        const {selectedCharacter} = this.state;

        return (
            <View style={FULL}>
                <Wallpaper/>
                <View
                  style={{flex: 1, flexDirection: 'row'}}
                >
                    {!isEmpty(characters.data) ? characters.data.map(character => (
                        <Character
                            key={character.type}
                            name={character.type}
                            isSelected={selectedCharacter === character.type}
                            onSelect={this.handleSelectCharacter}
                            onContinue={this.handleContinue}
                        />
                    )): null}
                </View>
            </View>
        )
    }
}
