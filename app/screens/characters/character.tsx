import * as React from 'react'
import {Animated, TouchableOpacity, View} from 'react-native'
import {Text} from '../../components/text'
import {spacing} from '../../theme';
import {Screen} from '../../components/screen';
import {Button} from '../../components/button';
import {ECharacter} from '../../services/api/api.enums';

export interface ICharacterProps {
    name: ECharacter;
    isSelected: boolean;
    onSelect: (name: ECharacter) => void;
    onContinue: (name: ECharacter) => void;
}

interface IState {
    containerWidth: Animated.Value;
    characterWidth: Animated.Value;
    descriptionWidth: Animated.Value;
}

export class Character extends React.Component<ICharacterProps, IState> {
    state: IState = {
        containerWidth: new Animated.Value(0),
        descriptionWidth: new Animated.Value(0),
        characterWidth: new Animated.Value(100),
    };

    componentDidUpdate (prevProps: ICharacterProps) {
        if (this.props.isSelected !== prevProps.isSelected) {
            Animated.timing(this.state.containerWidth, {
                toValue: this.props.isSelected ? 80 : 0,
                duration: 1000
            }).start();
            Animated.timing(this.state.descriptionWidth, {
                toValue: this.props.isSelected ? 65 : 0,
                duration: 1000
            }).start();
            Animated.timing(this.state.characterWidth, {
                toValue: this.props.isSelected ? 35 : 100,
                duration: 1000
            }).start();
        }
    }

    handleSelect = () => {
        this.props.onSelect(this.props.name);
    };

    handleContinue = () => {
        this.props.onContinue(this.props.name);
    };

    render() {
        const {name} = this.props;
        const {containerWidth, descriptionWidth, characterWidth} = this.state;
        let color = null;
        switch (name) {
            case ECharacter.JACK:
                color = 'red';
                break;
            case ECharacter.KATE:
                color = 'green';
                break;
            case ECharacter.JOHN:
                color = 'blue';
                break;
        }

        return (
            <Animated.View
                style={{
                    flex: 1,
                    backgroundColor: color,
                    flexBasis: containerWidth.interpolate({
                        inputRange: [0, 100],
                        outputRange: ['0%', '100%'],
                    }),
                    flexDirection: 'row',
                }}
            >
                <Animated.View
                    style={{
                        width: characterWidth.interpolate({
                            inputRange: [0, 100],
                            outputRange: ['0%', '100%'],
                        })
                    }}
                >
                    <TouchableOpacity
                        onPress={this.handleSelect}
                        style={{flex: 1, flexDirection: 'column'}}
                    >
                        <View style={{flex: 1, justifyContent: 'flex-end', paddingBottom: 20}}>
                            <Text style={{textAlign: 'center'}} text={name}/>
                        </View>
                    </TouchableOpacity>
                </Animated.View>

                <Animated.View
                    style={{
                        width: descriptionWidth.interpolate({
                            inputRange: [0, 100],
                            outputRange: ['0%', '100%'],
                        }),
                        backgroundColor: 'black'
                    }}
                >
                    <Screen
                        style={{
                            minWidth: 300
                        }}
                        preset="scroll"
                        backgroundColor={color.transparent}
                    >

                    </Screen>
                    <Button
                        style={{
                            paddingVertical: spacing[4],
                            paddingHorizontal: spacing[4],
                            backgroundColor: "#5D2555",
                            minWidth: 300,
                        }}
                        tx="firstExampleScreen.continue"
                        onPress={this.handleContinue}
                    />
                </Animated.View>
            </Animated.View>

        )
    }
}
