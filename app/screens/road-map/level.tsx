import {map, times} from 'lodash';
import * as React from 'react';
import {View, Text} from 'react-native'
import {ILevel} from '../../models/single-store/level';
import {Link} from './link';
import {ILink} from '../../models/single-store/link';
import {ELinkDirection} from '../../services/api/api.enums';
import {Direction} from './direction';

export interface IProps {
    level: ILevel;
}
const sortableDirections = [
    ELinkDirection.LEFT,
    ELinkDirection.UP,
    ELinkDirection.RIGHT
];

export class Level extends React.Component<IProps, {}> {

    renderLink (link: ILink, key) {
        return (
            <View
                key={key}
                style={{
                    display: 'flex',
                    flexDirection: 'row',
                    width: '20%',
                    justifyContent: 'center'
                }}
            >
                {link ? <Link link={link}/> : null}
            </View>
        );
    }

    render() {
        const {level} = this.props;

        return (
            <View>
                <View style={{
                    display: 'flex',
                    flexDirection: 'row',
                }}>
                    <Direction direction={null}/>
                    {level.links.reduce((result, link) => ([
                        ...result,
                        ...link.directions.map(direction => direction.direction).sort((a, b) => (
                            sortableDirections.indexOf(a) > sortableDirections.indexOf(b) ? 1 : -1
                        ))
                    ]), []).map((direction, key) => (
                        <Direction key={key} direction={direction}/>
                    ))}
                    <Direction direction={null}/>
                </View>
                <View style={{
                    display: 'flex',
                    flexDirection: 'row',
                }}>
                    {level.links.length === 3 ? this.renderLink(null, -1) : null}
                    {map(level.links, (link, key) => (
                        this.renderLink(link, key)
                    ))}
                    {level.links.length === 3 ? this.renderLink(null, 3) : null}
                </View>
            </View>
        )
    }
}
