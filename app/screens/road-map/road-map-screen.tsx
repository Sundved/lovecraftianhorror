import * as React from 'react';
import {View, ViewStyle} from 'react-native'
import {NavigationScreenProps} from 'react-navigation'
import {Wallpaper} from '../../components/wallpaper'
import {inject, observer} from 'mobx-react';
import {Screen} from '../../components/screen';
import {color} from '../../theme';
import {IRootStore} from '../../models/root-store';
import {ISingle} from '../../models/single-store/single';
import {Level} from './level';

const FULL: ViewStyle = { flex: 1 }

export interface ScreenProps extends NavigationScreenProps<{}> {
    single: ISingle;
}

interface IState {
}

@inject((stores: IRootStore) => ({
    single: stores.singleStore.single.data
}))
@observer
export class RoadMapScreen extends React.Component<ScreenProps, IState> {
    state: IState = {};

    render() {
        const {single} = this.props;

        return (
            <View style={FULL}>
                <Wallpaper/>
                <Screen

                    preset="scroll"
                    backgroundColor={color.transparent}
                >
                    <View
                        style={{display: 'flex', flexDirection: 'column'}}
                    >
                        {single.levels.map(level => (
                            <Level
                                key={level.number}
                                level={level}
                            />
                        ))}
                    </View>
                </Screen>
            </View>
        )
    }
}
