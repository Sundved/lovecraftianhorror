
import {map} from 'lodash';
import * as React from 'react';
import {Text, View} from 'react-native';
import {ILink} from '../../models/single-store/link';

export interface IProps {
    link: ILink;
}

export class BlankLink extends React.Component<IProps, {}> {

    render() {
        return (
            <View
                style={{
                    display: 'flex',
                    flexGrow: 1,
                    flexShrink: 0
                }}
            >
            </View>
        )
    }
}
