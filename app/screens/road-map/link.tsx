import {map} from 'lodash';
import * as React from 'react';
import {Text, View} from 'react-native';
import {ILink} from '../../models/single-store/link';

export interface IProps {
    link: ILink;
}

export class Link extends React.Component<IProps, {}> {

    render() {
        const {link} = this.props;

        return (
            <View style={{
                height: 50,
                width: 50,
                backgroundColor: 'yellow',
                borderRadius: 25
            }}>
                {map(link.directions, (direction, key) => (
                    <Text key={key}>{direction.direction}</Text>
                ))}
            </View>
        )
    }
}
