import * as React from 'react';
import Dash from 'react-native-dash';
import {View, ViewStyle} from 'react-native';
import {ELinkDirection} from '../../services/api/api.enums';

export interface IProps {
    direction: ELinkDirection;
}

export class Direction extends React.Component<IProps, {}> {

    render() {
        const {direction} = this.props;

        switch (direction) {
            case ELinkDirection.LEFT:
                return (
                    <View
                        style={{
                            display: 'flex',
                            height: 70,
                            width: '20%',
                            flexGrow: 1,
                            justifyContent: 'center'
                        }}
                    >
                        <Dash
                            dashGap={2}
                            dashLength={4}
                            dashThickness={2}
                            dashColor="yellow"
                            style={{
                                height: 2,
                                transform: [{ rotate: '45deg'}]
                            }}
                        />
                    </View>
                );
            case ELinkDirection.UP:
                return (
                    <View
                        style={{
                            display: 'flex',
                            height: 70,
                            width: '5%',
                            marginLeft: '-2.5%',
                            marginRight: '-2.5%',
                            flexGrow: 1,
                            justifyContent: 'center'
                        }}
                    >
                        <Dash
                            dashGap={2}
                            dashLength={4}
                            dashThickness={2}
                            dashColor="yellow"
                            style={{
                                height: 70,
                                flexDirection: 'column',
                                alignItems: 'center'
                            }}
                        />
                    </View>
                );
            case ELinkDirection.RIGHT:
                return (
                    <View
                        style={{
                            display: 'flex',
                            height: 70,
                            width: '20%',
                            flexGrow: 1,
                            justifyContent: 'center'
                        }}
                    >
                        <Dash
                            dashGap={2}
                            dashLength={4}
                            dashThickness={2}
                            dashColor="yellow"
                            style={{
                                height: 2,
                                transform: [{ rotate: '-45deg'}]
                            }}
                        />
                    </View>
                );
        }
        return (
            <View
                style={{
                    display: 'flex',
                    width: '10%',
                    flexGrow: 1
                }}
            >
            </View>
        );
    }
}
