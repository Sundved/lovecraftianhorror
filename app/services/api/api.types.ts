export interface UserWithToken {
    token: string;
    userId: string;
}

export interface Avatar {
    avatar: string;
}

