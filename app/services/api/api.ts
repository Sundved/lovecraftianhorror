import { ApisauceInstance, create, ApiResponse } from "apisauce"
import { ApiConfig, DEFAULT_API_CONFIG } from "./api-config"
import {
    UserWithToken,
    Avatar,
} from './api.types';
import {ImagePickerResponse} from 'react-native-image-picker';
import {ICharacterSnapshot} from '../../models/single-store/character';
import {IUserSnapshot} from '../../models/user-store';
import {IAncientSnapshot} from '../../models/single-store/ancient';
import {ISingleSnapshot} from '../../models/single-store/single';
import {EAncient, ECharacter} from './api.enums';

/**
 * Manages all requests to the API.
 */
export class Api {
  /**
   * The underlying apisauce instance which performs the requests.
   */
  apisauce: ApisauceInstance;

  /**
   * Configurable options.
   */
  config: ApiConfig;

  /**
   * Creates the api.
   *
   * @param config The configuration to use.
   */
  constructor(config: ApiConfig = DEFAULT_API_CONFIG) {
    this.config = config
  }

  /**
   * Sets up the API.  This will be called during the bootup
   * sequence and will happen before the first React component
   * is mounted.
   *
   * Be as quick as possible in here.
   */
  setup() {
    // construct the apisauce instance
    this.apisauce = create({
      baseURL: this.config.api,
      timeout: this.config.timeout,
      headers: {
        Accept: "application/json",
      },
    })
  }
  setAuthToken(token: string) {
    this.apisauce.setHeader('Authorization', `Bearer ${token}`);
  }

  loginUser (username: string, password: string): Promise<ApiResponse<UserWithToken>> {
      return this.apisauce.post('/auth/login', {
          username,
          password
      });
  }

  getUserInfo (): Promise<ApiResponse<IUserSnapshot>> {
      return this.apisauce.get('/users/info');
  }

  uploadAvatar = async({uri, fileName, type}: ImagePickerResponse): Promise<ApiResponse<Avatar>> => {
    let form = new FormData();
      form.append('file', {uri, type, name: fileName} as any);
      return this.apisauce.post('/files/avatar', form, {headers: {'Content-Type': 'multipart/form-data'}});
  };

  loadCharacters (): Promise<ApiResponse<ICharacterSnapshot[]>> {
    return this.apisauce.get('/characters');
  }

  loadAncients (): Promise<ApiResponse<IAncientSnapshot[]>> {
      return this.apisauce.get('/ancients');
  }

  newSingleGame (character: ECharacter, ancient: EAncient): Promise<ApiResponse<ISingleSnapshot>> {
      return this.apisauce.post('/single', {
          character,
          ancient,
      });
  }
    loadSingleGame (singleId): Promise<ApiResponse<ISingleSnapshot>> {
        return this.apisauce.get(`/single/${singleId}`);
    }
    breakSingleGame (): Promise<ApiResponse<void>> {
        return this.apisauce.delete('/single');
    }
}
