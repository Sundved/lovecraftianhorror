export enum ESingleGameStatus {
    ACTIVE = 'ACTIVE',
    WIN = 'WIN',
    DEFEAT = 'DEFEAT'
}

export enum EStatusType {
    IDLE = 'IDLE',
    PENDING = 'PENDING',
    DONE = 'DONE',
    ERROR = 'ERROR'
}

export enum ECharacter {
    JACK = 'JACK',
    KATE = 'KATE',
    JOHN = 'JOHN',
}

export enum EAncient {
    CTHULHU = 'CTHULHU',
    AZATHOTH = 'AZATHOTH',
    SHUB_NIGGURATH = 'SHUB_NIGGURATH',
}

export enum ELinkDirection {
    LEFT = 'LEFT',
    UP = 'UP',
    RIGHT = 'RIGHT'
}
